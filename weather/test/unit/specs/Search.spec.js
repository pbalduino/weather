import Vue from 'vue'
import Search from '@/components/Search'
import moxios from 'moxios'

const berlinData = JSON.stringify(
                { "coord": { "lon": 13.41, "lat": 52.52 },
                   "weather": [ { "id": 800,
                                  "main": "Clear",
                                  "description": "clear sky",
                                  "icon": "01n" } ],
                   "base": "stations",
                   "main": { "temp": 289.13,
                             "pressure": 1021,
                             "humidity": 72,
                             "temp_min": 288.15,
                             "temp_max": 290.15 },
                  "visibility": 10000,
                  "wind": { "speed": 3.1, "deg": 100 },
                  "clouds": { "all": 0 },
                  "dt": 1502763600,
                  "sys": { "type": 1,
                           "id": 4892,
                           "message": 0.0037,
                           "country": "DE",
                           "sunrise": 1502768950,
                           "sunset": 1502821875 },
                  "id": 2950159,
                  "name": "Berlin",
                  "cod": 200 });

const city = 'Berlin'
const country = 'DE'
const place = `${city},${country}`;
const clickEvent = new window.Event('click');
const apiKey = 'FAKEKEY';
const Constructor = Vue.extend(Search);

describe('Search component', () => {
  beforeEach(function () {
    require('es6-promise').polyfill();
    window.sessionStorage.clear();
    moxios.install();
  })

  afterEach(function () {
    moxios.uninstall()
  });

  it('should search for an existent city', done => {
    const Component = new Constructor({propsData: {apiKey: apiKey}}).$mount();
    const button = Component.$el.querySelector('button');

    moxios.withMock(() => {
      Component.place = place;
      Component.onButtonClick();

      Vue.nextTick(() => {
        moxios.wait(() => {
          let request = moxios.requests.mostRecent();
          request.respondWith({
            status: 200,
            responseText: berlinData
          })
          .then(() => {
            expect(Component.place).to.equal(place);
            expect(Component.result.name).to.equal(city);
            expect(Component.result.sys.country).to.equal(country);
            expect(Component.status).to.equal(Component.OK);

            expect(Component.result._fromCache_).to.be.false;

            done();
          })
          .catch(done);
        });
      });
    });
  });

  it('should display the weather at random geographic coordinates', done => {
    const Constructor = Vue.extend(Search);
    const Component = new Constructor({propsData: {apiKey: apiKey}}).$mount();

    moxios.withMock(() => {
      // Vue.nextTick(() => {
        moxios.wait(() => {
          let request = moxios.requests.mostRecent();
          request.respondWith({
            status: 200,
            responseText: berlinData
          })
          .then(() => {
            expect(Component.result.name).to.equal(city);
            expect(Component.result.sys.country).to.equal(country);
            expect(Component.status).to.equal(Component.OK);

            expect(Component.result._fromCache_).to.be.false;

            done();
          })
          .catch(done);
        });
      // });
    });
  });

  it('should cache url after the first call', done => {
    const Component = new Constructor({propsData: {apiKey: apiKey}}).$mount();
    const button = Component.$el.querySelector('button');

    moxios.withMock(() => {
      Component.place = place;
      Component.onButtonClick();

      Vue.nextTick(() => {
        moxios.wait(() => {
          let request = moxios.requests.mostRecent();
          request.respondWith({
            status: 200,
            responseText: berlinData
          })
          .then(() => {
            expect(Component.result._fromCache_).to.be.false;

            Component.onButtonClick();

            Vue.nextTick(() => {
              expect(Component.result._fromCache_).to.be.true;

              done();
            });
          })
          .catch(done);
        });
      });
    });
  });

  it('should fail for an inexistent city', done => {
    const Component = new Constructor().$mount();
    const button = Component.$el.querySelector('button');

    const city = 'Angu de caroço'
    const country = 'XXXXXXX'
    const place = `${city},${country}`;
    const fakeData = {"message": {"cod":"404","message":"city not found"}};

    moxios.withMock(() => {
      Component.apiKey = apiKey;
      Component.place = place;
      Component.onButtonClick();

      Vue.nextTick(() => {
        moxios.wait(() => {
          let request = moxios.requests.mostRecent();
          request.respondWith({
            status: 404,
            responseText: JSON.stringify(fakeData)
          })
          .then(() => {
            expect(Component.place).to.equal(place);
            expect(Component.result.cod).to.equal("404");
            expect(Component.status).to.equal(Component.ERROR);

            done();
          })
          .catch(done);
        });
      });
    });
  });
})
