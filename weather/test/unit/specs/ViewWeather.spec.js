import Vue from 'vue'
import ViewWeather from '@/components/ViewWeather'

const berlinData = { "coord": { "lon": 13.41, "lat": 52.52 },
                     "weather": [ { "id": 800,
                                    "main": "Clear",
                                    "description": "clear sky",
                                    "icon": "01n" } ],
                     "base": "stations",
                     "main": { "temp": 289.13,
                               "pressure": 1021,
                               "humidity": 72,
                               "temp_min": 288.15,
                               "temp_max": 290.15 },
                    "visibility": 10000,
                    "wind": { "speed": 3.1, "deg": 100 },
                    "clouds": { "all": 0 },
                    "dt": 1502763600,
                    "sys": { "type": 1,
                             "id": 4892,
                             "message": 0.0037,
                             "country": "DE",
                             "sunrise": 1502768950,
                             "sunset": 1502821875 },
                    "id": 2950159,
                    "name": "Berlin",
                    "cod": 200 };

describe('ViewWeather component', () => {
  beforeEach(function () {
    require('es6-promise').polyfill();
    window.sessionStorage.clear();
  });

  const Constructor = Vue.extend(ViewWeather);
  const Component = new Constructor({propsData: {weather: berlinData}}).$mount();

  describe('#round filter', () => {
    const round = Component.$options.filters.round;

    it('should round decimal numbers', () => {
      expect(round(3.14)).to.equal(3);
    });

    it('should not round non numbers', () => {
      expect(round()).to.be.NaN;
      expect(round('caninha')).to.be.NaN;
    });
  });

  describe('#utcTime filter', () => {
    const utcTime = Component.$options.filters.utcTime;

    it('should show timestamp as a UTC time', () => {
      expect(utcTime(berlinData.sys.sunrise)).to.equal('03:49 UTC');
      expect(utcTime(berlinData.sys.sunrise - (40 * 60))).to.equal('03:09 UTC');

      expect(utcTime(berlinData.sys.sunset)).to.equal('18:31 UTC');
    });

    it('should return empty when no timestamp is given', () => {
      expect(utcTime()).to.equal('');
    });
  });
});
