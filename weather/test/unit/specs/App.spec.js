import Vue from 'vue'
import App from '@/App'

describe('App component', () => {
  beforeEach(function () {
    require('es6-promise').polyfill();
    window.sessionStorage.clear();
  });

  it('should have an API key', () => {
    const Constructor = Vue.extend(App);
    const Component = new Constructor().$mount();

    expect(Component.apiKey).not.to.equal('');
  });

});
